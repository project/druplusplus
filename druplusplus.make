; Dru++ Makefile
  
; Core version
; ------------
core = 7.x
  
; API version
; ------------
api = 2
  
; Core project
; ------------
projects[drupal][version] = 7

  
  
; Modules

projects[content_access][version] = 1.2-beta1
projects[content_access][type] = "module"
projects[content_access][subdir] = "contrib"
projects[admin_menu][version] = 3.0-rc1
projects[admin_menu][type] = "module"
projects[admin_menu][subdir] = "contrib"
projects[ctools][version] = 1.x-dev
projects[ctools][type] = "module"
projects[ctools][subdir] = "contrib"
projects[context][version] = 3.0-beta2
projects[context][type] = "module"
projects[context][subdir] = "contrib"
projects[calendar][version] = 3.0-alpha1
projects[calendar][type] = "module"
projects[calendar][subdir] = "contrib"
projects[date][version] = 2.0-alpha5
projects[date][type] = "module"
projects[date][subdir] = "contrib"
projects[ds][version] = 1.4
projects[ds][type] = "module"
projects[ds][subdir] = "contrib"
projects[features][version] = 1.0-beta4
projects[features][type] = "module"
projects[features][subdir] = "contrib"
projects[email][version] = 1.0
projects[email][type] = "module"
projects[email][subdir] = "contrib"
projects[field_group][version] = 1.1
projects[field_group][type] = "module"
projects[field_group][subdir] = "contrib"
projects[link][version] = 1.0
projects[link][type] = "module"
projects[link][subdir] = "contrib"
projects[flag][version] = 2.0-beta6
projects[flag][type] = "module"
projects[flag][subdir] = "contrib"
projects[i18n][version] = 1.2
projects[i18n][type] = "module"
projects[i18n][subdir] = "contrib"
projects[auto_nodetitle][version] = 1.0
projects[auto_nodetitle][type] = "module"
projects[auto_nodetitle][subdir] = "contrib"
projects[backup_migrate][version] = 2.2
projects[backup_migrate][type] = "module"
projects[backup_migrate][subdir] = "contrib"
projects[captcha][version] = 1.0-beta1
projects[captcha][type] = "module"
projects[captcha][subdir] = "contrib"
projects[entity][version] = 1.0-rc1
projects[entity][type] = "module"
projects[entity][subdir] = "contrib"
projects[pathauto][version] = 1.0
projects[pathauto][type] = "module"
projects[pathauto][subdir] = "contrib"
projects[scheduler][version] = 1.0
projects[scheduler][type] = "module"
projects[scheduler][subdir] = "contrib"
projects[token][version] = 1.0-beta7
projects[token][type] = "module"
projects[token][subdir] = "contrib"
projects[transliteration][version] = 3.0
projects[transliteration][type] = "module"
projects[transliteration][subdir] = "contrib"
projects[relation][version] = 1.0-beta3
projects[relation][type] = "module"
projects[relation][subdir] = "contrib"
projects[rules][version] = 2.0
projects[rules][type] = "module"
projects[rules][subdir] = "contrib"
projects[page_title][version] = 2.5
projects[page_title][type] = "module"
projects[page_title][subdir] = "contrib"
projects[recaptcha][version] = 1.7
projects[recaptcha][type] = "module"
projects[recaptcha][subdir] = "contrib"
projects[wysiwyg][version] = 2.1
projects[wysiwyg][type] = "module"
projects[wysiwyg][subdir] = "contrib"
projects[views][version] = 3.0-rc3
projects[views][type] = "module"
projects[views][subdir] = "contrib"
projects[views_bulk_operations][version] = 3.0-beta3
projects[views_bulk_operations][type] = "module"
projects[views_bulk_operations][subdir] = "contrib"
projects[views_slideshow][version] = 3.0
projects[views_slideshow][type] = "module"
projects[views_slideshow][subdir] = "contrib"
projects[webform][version] = 3.15
projects[webform][type] = "module"
projects[webform][subdir] = "contrib"

  

; Themes
; --------
projects[omega][version] = 3.0
projects[omega][type] = "theme"

  
  
; Libraries
; ---------
libraries[ckeditor][type] = "libraries"
libraries[ckeditor][download][type] = "file"
libraries[ckeditor][download][url] = "http://download.cksource.com/CKEditor/CKEditor/CKEditor%203.6.2/ckeditor_3.6.2.tar.gz"

